<?php include("includes/header.php"); ?>
        
        <?php

$page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;

$items_per_page = 4;

$items_total_count = Graphics::count_all();

$graphics = Graphics::find_all();

$paginate = new Paginate($page, $items_per_page, $items_total_count);

$sql = "SELECT * FROM graphics ";
$sql .= "LIMIT {$items_per_page} ";
$sql .= "OFFSET {$paginate->offset()}";
$graphics = Graphics::find_by_query($sql);

?>

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
            
            <?php  foreach ($graphics as $graphic):  ?>
                
              
                
                <div class="thumbnails row">
                    
                    <div class="col-xs-6 col-md-3">
                        
                        <a class="thumbnail" href="graphic.php?id=<?php echo $graphic->id; ?>">
                           
                            
                            <img src="admin/<?php echo $graphic->graphic_path(); ?>" alt="" class="img-responsive home_page_graphic">
                            
                        </a>
                        
                    </div>
                                  <?php endforeach; ?>

                    
                </div>
                
                <div class="row">
                    
                        <ul class="pagination">
                           
                           <?php
                            
                            if($paginate->page_total() > 1) {
                                
                                if($paginate->has_next()) {
                                    
                                    echo "<li class='next'><a href='index.php?page={$paginate->next()}'>NEXT</a></li>";
                                    
                                }// end next
                                
                                
                                ?>
                                <?php
                                
                                for ($i=1; $i <= $paginate->page_total(); $i++) {
                                    
                                   if($i == $paginate->current_page) {
                                          
                                       echo "<li class='active'><a href='index.php?page={$i}'>{$i}</a></li>";

                                       
                                   } else {
                                       
                                      echo "<li ><a href='index.php?page={$i}'>{$i}</a></li>";

                                       
                                   }
                                    
                                    
                                }
                                    
                                    ?>
                                
                                
                                <?php
                                
                                      if($paginate->has_previous()) {

                            echo "<li class='previous'><a href='index.php?page={$paginate->previous()}'>PREVIOUS</a></li>";

                        } // end previous
                                
                            }  // end if
                            
                          

                  

          
                            
                            ?>
                            
                            
                            
                      
                        </ul>
                    
                </div>

            </div>

     

        <!-- /.row -->

        <?php include("includes/footer.php"); ?>
        
    
