<?php include("includes/init.php"); ?>
        <?php if(!$session->is_signed_in()) { redirect("login.php"); } ?>


        <!-- Navigation -->
<?php

    if(empty($_GET['id'])) {
        
        
        redirect("gallery.php");
    }

$graphic = Graphics::find_by_id($_GET['id']);

if($graphic) {
    
    
    $graphic->delete_graphic();
    
    redirect("gallery.php") ;
    
} else {
    
    
    redirect("upload.php");
}

?>

  <?php include("includes/footer.php"); ?>