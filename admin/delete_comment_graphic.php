<?php include("includes/init.php"); ?>
        <?php if(!$session->is_signed_in()) { redirect("login.php"); } ?>


        <!-- Navigation -->
<?php

    if(empty($_GET['id'])) {
        
        
        redirect("graphic_comment.php?id={$comment->graphic_id}");
    }

$comment = Comment::find_by_id($_GET['id']);

if($comment) {
    
    
    $comment->delete_comment();
    
    redirect("graphic_comment.php?id={$comment->graphic_id}") ;
    
} else {
    
    
    redirect("graphic_comment.php?id={$comment->graphic_id}");
}

?>

  <?php include("includes/footer.php?id={$comment->graphic_id}"); ?>