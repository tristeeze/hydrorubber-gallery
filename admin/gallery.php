<?php include("includes/header.php"); ?>
        
    <?php if(!$session->is_signed_in()) { redirect("login.php"); } ?>
    
    <?php
    
    $graphics = Graphics::find_all();
?>


        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->    
            
            
            <?php include("includes/top_nav.php"); ?>
            
           
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            
             <?php include("includes/sidebar.php"); ?>
             
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

                        <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           GALLERY
                            <small>Subheading</small>
                        </h1>
                       
                       <div class="col-md-12">
                           
                           <?php echo SITE_ROOT ; ?> 
                           <table class="table table-hover">
                               
                               <thead>
                                   <tr>
                                       <th>Graphic</th>
                                       <th>ID</th>
                                       <th>File Name</th>
                                       <th>Title</th>
                                       <th>File Size</th>
                                       <th>Comments</th>
                                   </tr>
                                   
                               </thead>
                               <tbody>
                                   <?php foreach ($graphics as $graphic) : ?>
                                   
                                   
                                   
                                   <tr>
                                       <td><img src="<?php echo $graphic->graphic_path(); ?>" class="admin-graphic-thumbnail" >
                                       
                                        <div class="graphic_line">
                                           <a href="delete_graphic.php?id=<?php echo $graphic->id ?>">Delete</a>
                                           <a href="edit_graphic.php?id=<?php echo $graphic->id ?>">Edit</a>
                                           <a href="../graphic.php?id=<?php echo $graphic->id; ?>">View</a>
                                       </div>
                                       
                                       </td>
                                      
                                       <td><?php echo $graphic->id; ?></td>
                                       <td><?php echo $graphic->graphic_filename; ?></td>
                                       <td><?php echo $graphic->graphic_title; ?></td>
                                       <td><?php echo $graphic->graphic_size; ?></td>
                                       <td>
                                          <a href="graphic_comment.php?id=<?php echo $graphic->id; ?>">
                                          <?php 
                                           
                                           $comment = Comment::find_comments($graphic->id);
                                           
                                           echo count($comment);

                                           
                                              ?></a></td>
                                      
                                   </tr>
                                   
                                   <?php endforeach; ?>
                               </tbody>
                               
                           </table> <!--END TABLE -->
                           
                       </div>
                       
                    </div>
                </div>
                <!-- /.row -->

            </div>

            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>