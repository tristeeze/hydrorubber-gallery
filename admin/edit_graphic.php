<?php include("includes/header.php"); ?>
        
    <?php if(!$session->is_signed_in()) { redirect("login.php"); } ?>
    
    <?php
    


if(empty($_GET['id'])) {
    
    redirect("gallery.php");
    
} else {
    
    
    $graphic = Graphics::find_by_id($_GET['id']);
    
    
    if(isset($_POST['update'])) {
        
    if($graphic) {
        
        
        $graphic->graphic_title         =  $_POST['title'];
        $graphic->graphic_caption       =  $_POST['caption'];
        $graphic->graphic_alt_text      =  $_POST['alt_text'];
        $graphic->graphic_description   =  $_POST['description'];
        
        $graphic->save();
    
    
}
    
}

}

?>


        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->    
            
            
            <?php include("includes/top_nav.php"); ?>
            
           
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            
             <?php include("includes/sidebar.php"); ?>
             
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

                        <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           GALLERY
                            <small>Subheading</small>
                        </h1>
                       
                       <div class="col-md-8">
                          
                        <form action="" method="post" class="form-group">
                          
                          
                           <div class="form-group">
                                <label for="caption">Title</label>
                                <input type="text" name="title" class="form-control" value="<?php echo $graphic->graphic_title; ?>">
                            </div>    
                           
                             <div class="form-group">
                             
                             <a href="#" class="thumbnail"><img  src="<?php echo $graphic->graphic_path(); ?>"></a>
                            </div>
                              
                           <div class="form-group">
                                <label for="caption">Caption</label>
                                <input  type="text" name="caption" class="form-control" value="<?php echo $graphic->graphic_caption; ?>">
                            </div>
                            
                                   
                           <div class="form-group">
                                <label for="caption">Alternate Text</label>
                                <input type="text" name="alt_text" class="form-control" value="<?php echo $graphic->graphic_alt_text; ?>">
                            </div>  
                            
                                                                  
                           <div class="form-group">
                                <label for="caption">Description</label>
    <textarea type="text" name="description" class="form-control" cols="30" rows="10" ><?php echo $graphic->graphic_description; ?></textarea>
                            </div>

                          
                       </div>
                                <div class="col-md-4" >
                            <div  class="graphic-info-box">
                                <div class="info-box-header">
                                   <h4>Save <span id="toggle" class="glyphicon glyphicon-menu-up pull-right"></span></h4>
                                </div>
                            <div class="inside">
                              <div class="box-inner">
                                 <p class="text">
                                   <span class="glyphicon glyphicon-calendar"></span> Uploaded on: April 22, 2030 @ 5:26
                                  </p>
                                  <p class="text ">
                                    Graphic Id: <span class="data graphic_id_box">34</span>
                                  </p>
                                  <p class="text">
                                    Filename: <span class="data">image.jpg</span>
                                  </p>
                                 <p class="text">
                                  File Type: <span class="data">JPG</span>
                                 </p>
                                 <p class="text">
                                   File Size: <span class="data">3245345</span>
                                 </p>
                              </div>
                              <div class="info-box-footer clearfix">
                                <div class="info-box-delete pull-left">
                                    <a  href="delete_graphic.php?id=<?php echo $graphic->id; ?>" class="btn btn-danger btn-lg ">Delete</a>   
                                </div>
                                <div class="info-box-update pull-right ">
                                    <input type="submit" name="update" value="Update" class="btn btn-primary btn-lg ">
                                </div>   
                              </div>
                            </div>          
                        </div>
                    </div>
                            </form>
                       
                    </div>
                    
                    
                </div>
                <!-- /.row -->

            </div>

            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>