<?php include("includes/header.php"); ?>
        
    <?php if(!$session->is_signed_in()) { redirect("login.php"); } ?>
    
    <?php
    
    if(empty($_GET['id'])) {
        
        redirect("users.php");
        
    } 

$user = User::find_by_id($_GET['id']);
    
    if(isset($_POST['update'])) {
        
        if($user) {
            
        $user->user_username = $_POST['username'];
        $user->user_firstname = $_POST['first_name'];
        $user->user_surname  = $_POST['surname'];
        $user->user_password = $_POST['password'];
            
            if(empty($_FILES['user_image'])) {
                
                $user->save();
                
            } else {
                
        $user->set_file($_FILES['user_image']);
        $user->upload_photo(); 
        $user->save();
        redirect("edit_user.php?id={$user->id}");
                
            }
            

           
        } 
}

?>


        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->    
            
            
            <?php include("includes/top_nav.php"); ?>
            
           
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            
             <?php include("includes/sidebar.php"); ?>
             
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

                        <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           GALLERY
                            <small>Subheading</small>
                        </h1>
                       
                      
                          <div class="col-md-6">
                              
                              <img class="img-responsive" src="<?php echo $user->image_path_placeholder(); ?>" alt="">
                              
                          </div>
                        <form action="" method="post" class="form-group" enctype="multipart/form-data">
                         <div class="col-md-6 ">
                         <div class="form-group">
                          <input type="file" name="user_image">
                            </div>
                           <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" name="username" class="form-control" value="<?php echo $user->user_username ?>" >
                            </div>    
                     
                              
                           <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input  type="text" name="first_name" class="form-control" value="<?php echo $user->user_firstname ?>">
                            </div> 
                              
                           <div class="form-group">
                                <label for="surname">Surname</label>
                                <input  type="text" name="surname" class="form-control" value="<?php echo $user->user_surname ?>">
                            </div>     
                              
                           <div class="form-group">
                                <label for="password">Password</label>
                                <input  type="password" name="password" class="form-control" value="<?php echo $user->user_password ?>">
                            </div>   
                               
                           <div class="form-group">
                               <a href="delete_user.php?id=<?php echo $user->id ?>" class="btn btn-danger">Delete</a>
                                <input  type="submit" name="update" class="btn btn-primary pull-right" value="UPDATE" >
                            </div>
                            
                                   
                        </div>
                            
                                                                  
                           </form>
                          
                                             <a href="delete_user.php?id=<?php echo $user->id ?>" class="btn btn-primary">Delete</a>

                              
                          
                       
                    </div>
                    
                    
                </div>
                <!-- /.row -->

            </div>

            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>