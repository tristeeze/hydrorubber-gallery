<?php


class Graphics extends Db_object {
    
    
    protected static $db_table = "graphics";
    protected static $db_table_fields = array('id','graphic_title','graphic_caption','graphic_description','graphic_alt_text','graphic_filename','graphic_type','graphic_size');
    public $id;
    public $graphic_title;
    public $graphic_caption;
    public $graphic_description;
    public $graphic_alt_text;
    public $graphic_filename;
    public $graphic_type;
    public $graphic_size;

    public $tmp_path;
    public $upload_directory = "uploads/graphics";
    public $errors = array();
    
    public $upload_errors_array = array(
    
    
        UPLOAD_ERR_OK           => "There is no error",
        UPLOAD_ERR_INI_SIZE     => "Upload file exceeds max upload",
        UPLOAD_ERR_FORM_SIZE    => "Upload file exceeds max file size",
        UPLOAD_ERR_PARTIAL      => "Only partcially uploaded",
        UPLOAD_ERR_NO_FILE      =>  "No file was uploaded",
        UPLOAD_ERR_NO_TMP_DIR   => "Missing temp folder",
        UPLOAD_ERR_CANT_WRITE   => "Failed to write to disk",
        UPLOAD_ERR_EXTENSION    => "PHP extention stopped the file from uploading"
    
    
    );
    
    
    public function set_file($file) {
        
        if(empty($file) || !$file || !is_array($file)) {
            
            $this->errors[] = "There was no file uploaded here";
            return false;
            
        } elseif($file['error'] !=0) {
            
            $this->errors[] = $this->upload_errors_array[$file['error']];
            return false;
            
        } else {
            
        $this->graphic_filename = basename($file['name']);
        $this->tmp_path = $file['tmp_name'];
        $this->graphic_type = $file['type'];
        $this->graphic_size = $file['size'];
 
        }
        
 
    } // END SET FILE
    
    
    public function graphic_path() {
    
        return $this->upload_directory.DS.$this->graphic_filename;
        
    }
    
    
    public function save(){
        
        if($this->id) {
            
            $this->update();
            
            
        } else {
            
            if(!empty($this->errors)) {
                return false;
            }
            
            if(empty($this->graphic_filename) || empty($this->tmp_path)) {
                
                $this->errors[] = "the file was not available";
                return false;
            }
            
            $target_path = SITE_ROOT .DS . 'admin' . DS . $this->upload_directory . DS . $this->graphic_filename;
            
            if(file_exists($target_path)) {
                
                $this->errors[] = "The file {$this->graphic_filename} already exists";
                
                return false;
            }
            
            if(move_uploaded_file($this->tmp_path, $target_path)) {
                
                if( $this->create()) {
                    
                    unset($this->tmp_path);
                    return true;
                    
                }
                
            } else {
                
                
                $this->error[] = "Error: Check folder permissions";
                
                return false;
            }
            
            $this->create();
        }
        
    } // END SAVE
    
    public function delete_graphic() {
        
        if($this->delete()) {
            
            $target_path = SITE_ROOT . DS . 'admin' . DS . $this->graphic_path();
             
            return unlink($target_path) ? true : false;
            
        } else {
            
            return false;
        }
        
    } // END DELETE
    
    
    
    
    
} // END CLASS

?>