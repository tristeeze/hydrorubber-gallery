<?php

class Comment extends Db_object {
    
    protected static $db_table = "comments";
    protected static $db_table_fields = array('id', 'graphic_id', 'comment_author', 'comment_body');
    public $id;
    public $graphic_id;
    public $comment_author;
    public $comment_body;

    
   public static function create_comment($graphic_id, $comment_author="JohnD", $comment_body="") {
       
   if(!empty($graphic_id) && !empty($comment_author) && !empty($comment_body)) {
           
        $comment = new Comment();

           $comment->graphic_id         = (int)$graphic_id;
           $comment->comment_author     = $comment_author;
           $comment->comment_body       = $comment_body;

        return $comment;
       
   }   else {
       
       return false;
   } 
   } // CREATE COMMETS

       public static function find_comments($graphic_id=0) {
           
           global $database;
    
        $sql = "SELECT * FROM " . self::$db_table;
        $sql.= " WHERE graphic_id = " . $database->escape_string($graphic_id);
        $sql.= " ORDER BY graphic_id ASC";

        return self::find_by_query($sql);
    
    
}       // FIND COMMENTS

        public function delete_comment() {
        
        if($this->delete()) {
            
           // $target_path = SITE_ROOT . DS . 'admin' . DS . $this->graphic_path();
             
            return unlink($target_path) ? true : false;
            
        } else {
            
            return false;
        }
        
    } // END DELETE
    
} // End of User Class







?>