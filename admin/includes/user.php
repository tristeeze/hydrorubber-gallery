<?php

class User extends Db_object {
    
    protected static $db_table = "users";
    protected static $db_table_fields = array('user_username', 'user_password', 'user_firstname', 'user_surname', 'user_image');
    public $id;
    public $user_username;
    public $user_password;
    public $user_firstname;
    public $user_surname;
    public $user_image;
    public $upload_directory = 'uploads/graphics';
    public $image_placeholder = 'http://placehold.it/400x400&text=profile_image';
    
    
    
    public function image_path_placeholder() {
        return empty($this->user_image) ? $this->image_placeholder : $this->upload_directory.DS.$this->user_image;
    }
 

    
    public static function verify_user($user_username, $user_password) {
        
    global $database;

    $username = $database->escape_string($user_username);
    $password = $database->escape_string($user_password);

        
    $sql = "SELECT * FROM "  . self::$db_table . " WHERE ";
    $sql .= "user_username = '{$user_username}' ";
    $sql .= "AND user_password = '{$user_password}' ";
    $sql .="LIMIT 1";

    $the_result_array = self::find_by_query($sql);

    return !empty($the_result_array) ? array_shift($the_result_array) : false; 
        
    }

    

       public function graphic_path() {
    
        return $this->upload_directory.DS.$this->user_image;
        
    } // END GRAPHIC PATH

        public function delete_user() {
        
        if($this->delete()) {
            
            $target_path = SITE_ROOT . DS . 'admin' . DS . $this->graphic_path();
             
            return unlink($target_path) ? true : false;
            
        } else {
            
            return false;
        }
        
    } // END DELETE
    
   

    
    public function upload_photo(){
        
//        if($this->id) {
//            
//            $this->update();
//            
//            
//        } else {
            
            if(!empty($this->errors)) {
                return false;
            }
            
            if(empty($this->user_image) || empty($this->tmp_path)) {
                
                $this->errors[] = "the file was not available";
                return false;
            }
            
            $target_path = SITE_ROOT .DS . 'admin' . DS . $this->upload_directory . DS . $this->user_image;
            
            if(file_exists($target_path)) {
                
                $this->errors[] = "The file {$this->user_image} already exists";
                
                return false;
            }
            
            if(move_uploaded_file($this->tmp_path, $target_path)) {
                
//                if( $this->create()) {
                    
                    unset($this->tmp_path);
                    return true;
                    
//                }
                
            } else {
                
                
                $this->error[] = "Error: Check folder permissions";
                
                return false;
            }
            
            $this->create();
//        }
        
    } // END SAVE

    
    
} // End of User Class







?>