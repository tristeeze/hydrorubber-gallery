<?php include("includes/header.php"); ?>
        
    <?php if(!$session->is_signed_in()) { redirect("login.php"); } ?>
    
    <?php

 $user = new User();
     
    if(isset($_POST['create'])) {
        
        if($user) {

        $user->user_username = $_POST['username'];
        $user->user_firstname = $_POST['first_name'];
        $user->user_surname  = $_POST['surname'];
        $user->user_password = $_POST['password'];
            
        $user->set_file($_FILES['user_image']);
        $user->upload_photo();    
        $user->save();    
            
        }

}


?>


        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->    
            
            
            <?php include("includes/top_nav.php"); ?>
            
           
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            
             <?php include("includes/sidebar.php"); ?>
             
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

                        <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           GALLERY
                            <small>Subheading</small>
                        </h1>
                       
                       <div class="col-md-6 col-md-offset-3">
                          
                        <form action="" method="post" class="form-group" enctype="multipart/form-data">
                        
                         <div class="form-group">
                          <input type="file" name="user_image">
                            </div>
                           <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" name="username" class="form-control" >
                            </div>    
                     
                              
                           <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input  type="text" name="first_name" class="form-control" >
                            </div> 
                              
                           <div class="form-group">
                                <label for="surname">Surname</label>
                                <input  type="text" name="surname" class="form-control" >
                            </div>     
                              
                           <div class="form-group">
                                <label for="password">Password</label>
                                <input  type="password" name="password" class="form-control" >
                            </div>   
                              
                           <div class="form-group">
                                <input  type="submit" name="create" class="btn btn-primary pull-right" >
                            </div>

                                                                  
                           </form>
                          
                       </div>
                              
                          
                       
                    </div>
                    
                    
                </div>
                <!-- /.row -->

            </div>

            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>