<?php include("includes/header.php"); ?>
        
    <?php if(!$session->is_signed_in()) { redirect("login.php"); } ?>
    
    <?php
    
    $User = User::find_all();
?>


        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->    
            
            
            <?php include("includes/top_nav.php"); ?>
            
           
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            
             <?php include("includes/sidebar.php"); ?>
             
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

                        <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           Users
                           
                           <a href="add_user.php" class="btn btn-primary">Add User</a>
                           
                        </h1>
                       
                       <div class="col-md-12">
                           
                           
                           <table class="table table-hover">
                               
                               <thead>
                                   <tr>
                                       <th>ID</th>
                                       <th>Photo</th>
                                       <th>Username</th>
                                       <th>First Name</th>
                                       <th>Surname</th>
                                   </tr>
                                   
                               </thead>
                               <tbody>
                                   <?php foreach ($User as $user) : ?>
                                   
                                   
                                   
                                   <tr>
                                      <td><?php echo $user->id; ?></td>
                                       <td><img class="admin-user-thumbnail user_image" src="<?php echo $user->image_path_placeholder(); ?>" alt="" ></td>
                                      
                                       
                                       <td><?php echo $user->user_username; ?>
                                       
                                           <div class="action_links">
                                           <a href="delete_user.php?id=<?php echo $user->id ?>">Delete</a>
                                           <a href="edit_user.php?id=<?php echo $user->id ?>">Edit</a>
                                       </div>
                                       
                                       </td>
                                       <td><?php echo $user->user_firstname; ?></td>
                                       <td><?php echo $user->user_surname; ?></td>
                                      
                                   </tr>
                                   
                                   <?php endforeach; ?>
                               </tbody>
                               
                           </table> <!--END TABLE -->
                           
                       </div>
                       
                    </div>
                </div>
                <!-- /.row -->

            </div>

            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>