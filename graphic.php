<?php include("includes/header.php"); ?>
<?php


require_once("admin/includes/init.php");



if(empty($_GET['id'])) {
    
    redirect("index.php");
}

$graphic = Graphics::find_by_id($_GET['id']);

if(isset($_POST['submit'])) {
    
    $comment_author = trim($_POST['author']);
    $comment_body = trim($_POST['body']);
    
    $new_Comment = Comment::create_comment($graphic->id,$comment_author, $comment_body);
    
    if($new_Comment && $new_Comment->save()) {
        
        
    redirect("graphic.php?id={$graphic->id}");
        
    } else {
        
        $message = "ERRORS ON PAGE";
    }
    
} else {
    
    
    $comment_author = "";
    $comment_body = "";
}

$comments = Comment::find_comments($graphic->id);



?>


<div class="row">
            <div class="col-lg-12 ">

                <!-- Blog Post -->

                <!-- Title -->
                <h1><?php  echo $graphic->graphic_title; ?></h1>

                <!-- Author -->
                <p class="lead">
                    by <a href="#">Start Bootstrap</a>
                </p>

                <hr>

                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> Posted on August 24, 2013 at 9:00 PM</p>

                <hr>

                <!-- Preview Image -->
                <img class="img-responsive" src="admin/<?php echo $graphic->graphic_path(); ?>" alt="">

                <hr>

                <!-- Post Content -->
                <p class="lead"><?php  echo $graphic->graphic_caption; ?></p>                
                <p class="lead"><?php  echo $graphic->graphic_description; ?></p>

                

                <hr>

                <!-- Blog Comments -->

                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form role="form" method="post">
                        <div class="form-group">
                        <label for="author">Author</label>
                        <input type="text" name="author" class="form-control">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="body" rows="3"></textarea>
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                
                
                
                

                <hr>

                <!-- Posted Comments -->
                
                <?php foreach ($comments as $comment): ?>
    
    


                <!-- Comment -->
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading"><?php echo $comment->comment_author; ?>
                            <small>August 25, 2014 at 9:30 PM</small>
                        </h4>
                     <?php echo $comment->comment_body; ?>
                    </div>
                </div>
                
                <?php endforeach; ?>
                

                <!-- Comment -->
                

            </div>
            
            </div>
            <!-- Blog Sidebar Widgets Column -->
 
        <!-- /.row -->

        <?php include("includes/footer.php"); ?>

            